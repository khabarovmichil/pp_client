import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Request {

    String uri;
    String payload;
    String uuid;

    long timestamp;

    Map<String, String> headers;

    Request(String uri, String payload){
        this.uri = uri;
        this.payload = payload;
        this.uuid = UUID.randomUUID().toString();
        this.timestamp = System.currentTimeMillis();

        initHeaders();

    }



    private void initHeaders() {
        headers = new HashMap<>() {};

        addHeader("X-ORG-ID", "PHONEPE");
        addHeader("accept", "application/json");
        addHeader("X-SOURCE-TYPE", "APP");
        addHeader("X-SOURCE-PLATFORM", "Android");
        addHeader("X-SOURCE-VERSION", "24032203");
        addHeader("X-MERCHANT-ID", "FXM");
        addHeader("X-APP-ID", "fdab17054b934ee28271230cb743d3a7");
        addHeader("X-DG-G", "c734d142919936c8b21d8066340a387fdc323c299c7f2a6e60cc2f83099fa3f5");
        addHeader("X-SOURCE-LOCALE", "en");
        addHeader("X-COMPRESSION-ALGORITHM", "gzip");
        //addHeader("X-Device-Fingerprint", "0000000684o5Lljc0j+o18YiOfoo1zUrS/uZMP5OAQ5fwO2U6kDSphF6Nd6NN+avUgCO7bEmou++VKm+rpnhGMlBpAVJI/N2ktXRA8mt+pulhVXH6vklQDLc7a+Op0x3HufLVq7ABccy0XYhsUG7mhCCY6H0plqA+0KkZLEoWDq7uBTqrKEJT/CxJcAAgwjgWk5TFS7W7WgVjLTixsmwTeYu+TIr2aIOizAZdCXsX0Xe3oQRrjT+SuBoJp792i37oDV022YGlK7XE+hmsX2SyxnLxKkU2aeRMkLjZYkyLH5pystKEd5ZHgjkxRAA/BSlwbAIuB5X8IrjDSQM64YgORwwIZ1hGyBmdfjAdjZN0PjH/UrpVu9PX1ouM2xXlc8FqBdBfVQqSvzpfl7sX8lEiDE8Iy9+7LpVGWzyLEhJXbPv1VNCJovnyDqXO3sV1mdqdi0RC4DzxH5LfLR+NdA7O49IAWdW3J2oEWmJB+GP6IH//7ujDz7dZhdt9nHg7+hnpXP54mMwcIq6cU5xzLZdj1i9JdX2frH3LwmnJbh0eDsi4LAv2jCWx6sMrz2qdy5KRGn7ouxj3pF53ilu2Y/AMRoOSTs6HYIek+CTNviYQCYPUxa4gJRRv/4c0qjEXVqCPsEf69o399gz06Mn0gqZcnNnsBoy3MfODhh
        addHeader("X-CHECKMATE-CLIENT-ID", "ANDROID");
        addHeader("X-CHECKMATE-KEY-VERSION", "3");
        addHeader("X-REQUEST-START-TIME", String.valueOf(timestamp));
        addHeader("Content-Type", "application/json; charset=utf-8");
        addHeader("Accept-Encoding", "gzip");
        addHeader("User-Agent", "okhttp/4.9.3");
    }

    void addHeader(String key, String value){
        headers.put(key, value);
    }


}