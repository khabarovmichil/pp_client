import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ProtocolException;
import java.net.URL;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;

public class Network {

    static String serverAddress = "https://apicp1.phonepe.com";

    Request request;

    Signer signer = new Signer();

    public Network(Request request) throws Exception {
        this.request = request;
        signer.sign(request);
        //X-Device-Fingerprint

        String deviceUUID = "739ad791-4c92-4816-8bb5-21bb981f198c";
        request.addHeader("X-Device-Fingerprint", getXDeviceId(deviceUUID));

    }

    String getXDeviceId(String uuid) throws Exception {

        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(256);
        SecretKey c4 = keyGenerator.generateKey();


        System.out.println(c4.getAlgorithm());
        String b16 = b(c4, Base64.getEncoder().encodeToString(uuid.getBytes()).getBytes());


        if (b16 != null) {


            String m16 = m(Base64.getEncoder().encodeToString(c4.getEncoded()));

            if (m16 != null) {
                String j15 = j(Integer.toString(m16.length()));
                return  j15 + m16 + b16;
            }
        }


        throw new RuntimeException("Не удалось получить XDeviceId");
    }

    public String b(SecretKey secretKey, byte[] bArr) throws Exception {
        SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getEncoded(), "AES/CBC/PKCS5Padding");
        IvParameterSpec ivParameterSpec = new IvParameterSpec(Arrays.copyOf(secretKey.getEncoded(), 16));
        BouncyCastleProvider bouncyCastleProvider = new BouncyCastleProvider();
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", bouncyCastleProvider);
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
        return Base64.getEncoder().encodeToString(cipher.doFinal(bArr));
    }


    public String m(String str) throws Exception {
        String publicKey = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAqBp3kP7FbFjl4Vy9DysqUeHsCB3nCUY5fF0PbwLmYQwVWgeziH2hfwYFYDGqK41eaEt4HAl7+U9uQpmIkkrk+tPlq4tpaqS3WCocCIY1wmeHEECX4yso6eja0U9j7B+G84ClrOqIpq7KqpZpIqcXy84bfY5wXcru3ulCRhO+o6OokORO73apHS3xDiqeXfwLwNRn+LWc8PXUYHXG0vkEMdEPaSXGdf1Qrt3CnKop2XE+SYEK5IEHuDLMeqRXdXfFIJrIWk8sj8rW5OMk2u4RYFKT9ZRUw0Bqmoud284i5wAIGIfvdTsnbLUyAhHi3SE+2LydU7aco0+MWcW2ULMFONDT8qTW6JXmln1t48zD8B2d/KodaBHLrO5Pa1Ftnp2bKvJAIgX1UcTYyuvVQ/02XWVIHp7UwXjuYckPVCgppymQoafUqI+Snue/8IrvvSe/s40W8DD7XjZf3phyTmV3SMpux+Gul1paa1B2+DqYZm8KoQ56DpoppXafAMpVqLeNsP7oKbNTdnkNQe7M2mX367KNBIeX8nhn1kbBJDgdMCGiTjWwxo1xccDp0Xx4teZRNdFLU7hcFI65W8eNmmLgTFm8xKiqDHtrtbncOUieaS8v+L7AnVhEV3o6VZe/YWFkEZ8q9NhnwzBB06aLVCLmdvGdF5V7eHZ3GKyAjuNggncCAwEAAQ==";
        PublicKey key = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(Base64.getDecoder().decode(publicKey)));
        BouncyCastleProvider bouncyCastleProvider = new BouncyCastleProvider();
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1PADDING", bouncyCastleProvider);
        cipher.init(1, key);
        return Base64.getEncoder().encodeToString(cipher.doFinal(str.getBytes()));
    }




    public String j(String str) {
        return "0000000000".substring(str.length()) + str;
    }



    public void send() {

        try {
            URL url = new URL(serverAddress + request.uri);


            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestMethod("POST");

            // Добавляем заголовки
            request.headers.forEach(connection::setRequestProperty);

            System.out.println("URL: " + connection.getURL());
            System.out.println("Method: " + connection.getRequestMethod());
            System.out.println("Payload: " + request.payload);
            System.out.println("UUID: " + request.uuid);
            System.out.println("Headers: ");
            for (String key : request.headers.keySet()) {
                System.out.println(key + ": " + request.headers.get(key));
            }


            connection.setDoOutput(true);
            OutputStream outputStream = connection.getOutputStream();
            outputStream.write(request.payload.getBytes());
            outputStream.flush();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            StringBuilder response = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            reader.close();
            System.out.println("Code: " + connection.getResponseCode());
            System.out.println("Response: " + response);
            connection.disconnect();
        } catch (ProtocolException e) {
            throw new RuntimeException("Протокол не поддерживается");
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Ошибка ввода/вывода");
        }
    }

}
