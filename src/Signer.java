import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.*;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class Signer {

    static byte[] keystorePublicKey = {48, -126, 3, -127, 48, -126, 2, 105, -96, 3, 2, 1, 2, 2, 4, 120, -52, 83, -14, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 11, 5, 0, 48, 112, 49, 11, 48, 9, 6, 3, 85, 4, 6, 19, 2, 73, 78, 49, 18, 48, 16, 6, 3, 85, 4, 8, 19, 9, 75, 97, 114, 110, 97, 116, 97, 107, 97, 49, 18, 48, 16, 6, 3, 85, 4, 7, 19, 9, 66, 97, 110, 103, 97, 108, 111, 114, 101, 49, 16, 48, 14, 6, 3, 85, 4, 10, 19, 7, 80, 104, 111, 110, 101, 80, 101, 49, 16, 48, 14, 6, 3, 85, 4, 11, 19, 7, 80, 104, 111, 110, 101, 80, 101, 49, 21, 48, 19, 6, 3, 85, 4, 3, 19, 12, 86, 105, 118, 101, 107, 32, 83, 111, 110, 101, 106, 97, 48, 32, 23, 13, 49, 54, 48, 54, 48, 49, 48, 57, 48, 48, 48, 50, 90, 24, 15, 50, 48, 54, 54, 48, 53, 50, 48, 48, 57, 48, 48, 48, 50, 90, 48, 112, 49, 11, 48, 9, 6, 3, 85, 4, 6, 19, 2, 73, 78, 49, 18, 48, 16, 6, 3, 85, 4, 8, 19, 9, 75, 97, 114, 110, 97, 116, 97, 107, 97, 49, 18, 48, 16, 6, 3, 85, 4, 7, 19, 9, 66, 97, 110, 103, 97, 108, 111, 114, 101, 49, 16, 48, 14, 6, 3, 85, 4, 10, 19, 7, 80, 104, 111, 110, 101, 80, 101, 49, 16, 48, 14, 6, 3, 85, 4, 11, 19, 7, 80, 104, 111, 110, 101, 80, 101, 49, 21, 48, 19, 6, 3, 85, 4, 3, 19, 12, 86, 105, 118, 101, 107, 32, 83, 111, 110, 101, 106, 97, 48, -126, 1, 34, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 1, 5, 0, 3, -126, 1, 15, 0, 48, -126, 1, 10, 2, -126, 1, 1, 0, -127, 62, -63, 106, 68, 99, 126, -85, 21, 81, 70, -97, 63, 69, 67, 0, -89, -94, 6, -33, 81, 121, -70, 119, 80, 66, -11, -119, -27, 115, -4, 13, 17, -1, 13, -104, -116, -83, -34, 37, -72, -55, -48, -106, -3, 62, 91, -91, 74, -36, -10, -40, 26, -25, -97, -128, 110, 115, -99, -109, 27, 101, -101, 24, -105, 41, 117, 121, 24, -89, 61, -32, -34, -91, -62, -32, -32, 6, 19, -30, 92, 98, -74, -48, 46, 19, -93, -66, -7, 10, -10, 28, 25, -8, -56, 127, -128, 33, 36, 57, 32, -38, 5, -33, -18, -54, 114, -45, 34, -107, 89, -57, 59, -90, -104, -49, -90, -101, 78, -118, -74, 42, 77, -21, 51, 45, 86, 10, -56, 40, -60, -65, -120, 78, 2, -78, 47, 80, 74, -67, -80, 63, -42, 38, 8, 63, 117, -118, 41, 100, 41, 90, 115, -89, -54, -63, -33, 47, -104, -69, -122, -6, -7, -94, -11, -24, -56, -10, -68, 101, -43, 91, 3, -13, -76, 62, -119, 103, 68, 22, -66, -57, 120, -7, -115, -84, -16, 35, 13, 55, -15, -66, 78, 80, 11, -60, -122, 30, -104, -40, -41, -121, 30, 94, -58, -63, -47, -40, -88, 113, -24, -52, -15, 8, 54, 20, 119, -80, -59, 27, 14, 119, -39, 36, 65, -89, 16, 11, 102, -44, -116, -21, 30, -15, 61, 42, -16, -2, 25, -13, -52, 86, -90, -86, 9, -47, -118, -78, -17, -42, 23, 109, 45, -2, 10, 7, 2, 3, 1, 0, 1, -93, 33, 48, 31, 48, 29, 6, 3, 85, 29, 14, 4, 22, 4, 20, -84, 22, -108, 84, 90, -123, -78, -12, -45, -116, -56, 95, -24, -118, 44, 9, -21, -58, 3, -76, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 11, 5, 0, 3, -126, 1, 1, 0, 10, -115, -88, -84, -10, 62, 28, 76, 119, -105, 12, 1, 98, -80, 10, -48, 61, 121, 99, -122, -11, -21, 38, -119, -48, -107, 3, 19, -71, 67, 125, 80, -11, -43, -58, -29, 35, -101, -127, 53, -112, 41, -24, -71, -8, -68, -14, 89, 127, -71, 77, 5, -103, -27, -60, -30, -13, -108, 57, 24, 4, -110, 101, 25, 9, -83, 82, -64, 117, -84, -15, -120, 60, 83, -18, 109, 82, 36, -5, 75, 43, 44, 98, -37, -116, -104, 31, 92, 36, 59, 62, 63, -94, 1, -23, -27, -30, -69, -3, -6, -72, -89, 12, -121, -59, -74, -110, 0, 89, -61, 77, 65, -91, 115, 16, -77, 111, 124, -74, -47, -74, -16, 73, -128, -112, -125, -48, 16, -15, 104, -123, -83, 80, -8, -2, 113, -111, 106, 4, 96, 69, 51, 110, -54, 44, -76, 75, -9, 6, -108, -122, -89, 65, -33, -13, 113, -57, -52, 58, -98, -65, 119, 20, -48, -1, 100, -67, -82, -83, -27, -125, 81, -54, 51, -27, 59, 115, -22, 96, -94, 90, -105, -37, -112, 50, 39, 76, -29, -102, -2, -24, -53, 70, -101, -50, -12, 105, -96, 32, -51, -53, 125, -123, 49, 18, -60, 12, -52, -84, 113, 15, 62, -124, -36, 36, 117, 21, -34, 6, -9, -72, -107, -36, -45, 40, 50, -69, 99, -102, 54, 114, -70, 31, 114, 105, 125, -103, -30, 26, 37, 105, 83, 62, 36, 100, 70, -119, 63, -31, -128, -61, -43, -118, -12, 68, 43};

    SecretKey xDeviceIdEncryptionKey;




    void sign(Request request){

        try {
            byte[] tempEnv = getJByteArray(6);
            byte[] sha1PublicKey = sha1(keystorePublicKey);
            byte[] baseEncoded = base64Encode(sha1PublicKey);

            byte[] signature = concatBytes(request.uuid.getBytes(), baseEncoded);


            byte[] sha256EncryptedPayload = sha256(concatBytes(request.uri.getBytes(), request.payload.getBytes()));
            byte[] encryptedPayloadEnv = base64Encode(sha256EncryptedPayload);

            byte[] timestamp = String.valueOf(request.timestamp).getBytes();

            byte[] timestampFilledAndPayload = concatBytes(timestamp, tempEnv, encryptedPayloadEnv);

            byte[] encryptedTotal = encryptAes(timestampFilledAndPayload, signature);

            byte[] base64EncodedTimestampFilledAndPayload = Base64.getEncoder().encode(encryptedTotal);


            byte[] resultsArray = concatBytes(request.uuid.getBytes(), tempEnv, base64EncodedTimestampFilledAndPayload);
            byte[] data = decryptAes(resultsArray, signature);


            System.out.println(new String(data));


        } catch (Exception e) {
            e.printStackTrace();
        }
    }





    public static byte[] getJByteArray(int size) {
        // Создаем массив байтов заданного размера
        byte[] array = new byte[size];
        // Заполняем массив определенным значением
        byte value = (byte)(size & 0xFF);
        for (int i = 0; i < size; i++) {
            array[i] = value;
        }
        return array;
    }



    public static byte[] concatBytes(byte[]... arrays) {
        // Вычисляем общую длину всех массивов
        int totalLength = 0;
        for (byte[] array : arrays) {
            if (array != null) {
                totalLength += array.length;
            }
        }

        // Создаем новый массив с общей длиной
        byte[] result = new byte[totalLength];
        int offset = 0;

        // Копируем содержимое каждого массива в новый массив
        for (byte[] array : arrays) {
            if (array != null) {
                System.arraycopy(array, 0, result, offset, array.length);
                offset += array.length;
            }
        }

        return result;
    }


    byte[] decryptAes(byte[] input, byte[] key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        SecretKeySpec secretKeySpec = new SecretKeySpec(Arrays.copyOfRange(key, 0, 16), "AES");
        BouncyCastleProvider bouncyCastleProvider = new BouncyCastleProvider();
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding", bouncyCastleProvider);
        cipher.init(2, secretKeySpec);
        return cipher.doFinal(input);
    }


     byte[] encryptAes(byte[] input, byte[] key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        SecretKeySpec secretKeySpec = new SecretKeySpec(Arrays.copyOfRange(key, 0, 16),  "AES");
         BouncyCastleProvider bouncyCastleProvider = new BouncyCastleProvider();

         Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding", bouncyCastleProvider);
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
        return cipher.doFinal(input);
    }





    byte[] base64Encode(byte[] bArr){
        Base64.Encoder encoder = Base64.getEncoder();
        // TODO: Проверить. Точно ли NO_WRAP
        return encoder.encode(bArr);
    }


    byte[] sha1(byte[] bArr) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
        messageDigest.update(bArr);
        return messageDigest.digest();
    }

    byte[] sha256(byte[] bArr) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update(bArr);
        return messageDigest.digest();
    }

}
