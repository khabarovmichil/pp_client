import java.io.IOException;
import java.util.UUID;



public class PhonePeClient {



    static void bulk() throws Exception {

        String body = "{\n" +
                "  \"appContext\": {\n" +
                "    \"osVersion\": \"29\",\n" +
                "    \"buildTrack\": \"RELEASE\",\n" +
                "    \"deviceModel\": \"Android SDK built for arm64\",\n" +
                "    \"location\": {\n" +
                "      \"latitude\": 0.0,\n" +
                "      \"longitude\": 0.0\n" +
                "    },\n" +
                "    \"appversion\": \"24.03.22.0\",\n" +
                "    \"deviceManufacturer\": \"Google\",\n" +
                "    \"versionCode\": \"24032203\",\n" +
                "    \"buildGenerationTime\": \"1711564200000\",\n" +
                "    \"ckv\": \"6\"\n" +
                "  },\n" +
                "  \"keyCrisp\": [\n" +
                "    {\n" +
                "      \"key\": \"consumer_app_login_configs\",\n" +
                "      \"version\": 0\n" +
                "    }\n" +
                "  ],\n" +
                "  \"maxSize\": 25\n" +
                "}";

        Request request = new Request("/apis/chimera/pz/v1/whitelisted/Apps/evaluate/bulk", body);

        Network network = new Network(request);
        network.send();

    }



}
